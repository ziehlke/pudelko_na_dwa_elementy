import box.GenericBox;
import box.GenericBoxNaWieleElementow;

public class Main {
    public static void main(String[] args) {

/*
        GenericBox<String> boxStringow = new GenericBox<>("pierwszy");
        boxStringow.addObject("drugi");
*/

        GenericBox<Integer> boxIntow = new GenericBox<>(1);
        boxIntow.addObject(2);


//        System.out.println(boxStringow);
        System.out.println(boxIntow);


        GenericBoxNaWieleElementow<String> genericBoxNaWieleElementow = new GenericBoxNaWieleElementow<>();
//        genericBoxNaWieleElementow.addItem(1);
        genericBoxNaWieleElementow.addItem("pierwszy");
        genericBoxNaWieleElementow.addItem("drugi");
        genericBoxNaWieleElementow.addItem("trzeci");

        System.out.println(genericBoxNaWieleElementow.toString());
    }
}
