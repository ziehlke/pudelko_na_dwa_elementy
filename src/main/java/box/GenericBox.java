package box;

public class GenericBox <T extends Number> {

    private T item;
    private T item2;


    public GenericBox(T item) {
        this.item = item;
    }


    public void addObject(T item) {
        this.item2 = item;
    }


    @Override
    public String toString() {
        return "GenericBox{" +
                "item=" + item +
                ", item2=" + item2 +
                '}';
    }
}
