package box;

import java.util.ArrayList;

public class GenericBoxNaWieleElementow<T> {

    private ArrayList objects;


    public GenericBoxNaWieleElementow() {
        objects = new ArrayList();
    }


    public void addItem(T item) {
        objects.add(item);
    }


    @Override
    public String toString() {
        return "GenericBoxNaWieleElementow{" + objects + " }";
    }
}
