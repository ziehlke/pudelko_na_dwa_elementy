package generyczne_metody;

import java.util.Arrays;
import java.util.Collection;

public class Najmniejszy {


    public static void main(String[] args) {
        Integer[] arr = {234, 234234, 253325, 234, 243};

        System.out.println(findMinimum(Arrays.asList(arr)));

    }


    private static <T extends Number & Comparable<T>> T findMinimum(Collection<T> collection) {

        T minimum = collection.iterator().next();


        for (T number : collection) {
            if (number.compareTo(minimum) < 0) {
                minimum = number;
            }


        }

        return minimum;

    }


}
